var knex = require('./postgres');

function generalHash(q) {
  return {
    app_key: q.app_key,
    device_id: q.device_id,
    old_device_id: q.old_device_id,

    created_at: new Date(q.timestamp * 1000),
    updated_at: new Date(q.timestamp * 1000),

    hour: q.hour,
    day_of_week: q.dow,

    country_code: q.country_code,
    city: q.city,
    location: q.location,
  };
}

function beginSession(q) {
  return {
    type: 'begin_session',
    data: {
      ip_address: q.ip_address,
      metrics: q.metrics,
    },
  };
}

function sessionDuration(q) {
  return {
    type: 'session_duration',
    data: {
      duration: q.session_duration,
    },
  };
}

function endSession(q) {
  return {
    type: 'end_session',
  };
}

function event(event) {
  var h = {
    type: 'event',
    event: event.key,
    count: event.count,
    sum: event.sum,
    duration: event.dur,
    data: event.segmentation,
  };

  if ('dow' in event) h.day_of_week = event.dow;
  if ('hour' in event) h.hour = event.hour;

  return h;
}

function userDetails(q) {
  return { data: q.user_details };
}

module.exports = function(query) {
  var general = generalHash(query);
  var rows = [];

  function row(row) {
    rows.push(Object.assign({}, general, row));
  }

  if (query.begin_session) {
    row(beginSession(query));
  }

  if (query.session_duration) {
    row(sessionDuration(query));
  }

  if (query.end_session) {
    row(endSession(query));
  }

  if (query.events) {
    var events = JSON.parse(query.events);

    for (var i = 0; i < events.length; i++) {
      var e = events[i];
      row(event(e));
    }
  }

  if (query.user_details) {
    row(userDetails(query));
  }

  return knex.batchInsert('countly_log', rows).returning('id');
};
