var knex = require('./postgres');

console.log('Creating table if it doesn\'t exist');

knex.schema.createTableIfNotExists('countly_log', function(table) {
  table.increments();

  table.string('app_key');
  table.string('device_id');
  table.string('old_device_id');

  table.string('type');
  table.string('event');

  table.double('sum');
  table.double('duration');
  table.integer('count');

  table.integer('hour');
  table.integer('day_of_week');

  table.string('country_code');
  table.string('city');
  table.string('location');

  table.jsonb('data');

  table.timestamps();
  console.log('Done.');
}).then(function() {
  console.log('GOOD!');
}, function(err) {

  console.log('Err: ', err);
}).finally(function() {
  process.exit();
});

console.log('Done?');
