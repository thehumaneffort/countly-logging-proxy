var koa = require('koa');
var error = require('koa-error');
var hyperquest = require('hyperquest');

var forwardToPg = require('./forward_to_pg');
var app = koa();

var errors = require('./errors');

app.use(error());
app.use(require('detour-router').middleware);

app.use(function *(next) {
  // Request is a write operation:
  if (this.request.path.match(/^\/i(\/|$)/)) {
    console.log('Incoming Write Request: ' + this.request.path);

    var q = this.request.query;

    try {
      pg_res = yield forwardToPg(q);
    } catch (x) {
      console.error(x);
      errors.report(x);
    }
  }

  yield next;
});

const FWD_OPTIONS = {
  headers: {
    'x-real-ip': undefined,
    'x-forwarded-proto': undefined,
    'x-forwarded-for': undefined,
    host: 'analytics.yogi-tunes.com',
  },
};

app.use(function *(next) {
  var countlyRequest;

  if (this.request.path == '/i' ||
     this.request.path == '/o' ||
     this.request.path.indexOf('/i/') == 0 ||
      this.request.path.indexOf('/o/') == 0) {
    countlyRequest = this.detour.forwardTo(process.env.COUNTLY_API_URL, FWD_OPTIONS);
  } else {
    countlyRequest = this.detour.forwardTo(process.env.COUNTLY_FRONTEND_URL, FWD_OPTIONS);
  }

  var countlyResponse = yield this.detour.responseFor(countlyRequest);

  this.detour.pipeFrom(countlyRequest, countlyResponse);
});

console.log('Listening to 80 on fff bindings');
app.listen(80);

// out stuff:

// t.string :app_key
// t.string :device_id
// t.string: old_device_id // preserve chains
// t.string :type  // [begin_session,session_duration,end_session,metrics,event,user_detail]

// t.string :event // [whatever kinds of events, null if non-event]
// h.decimal :sum
// h.integer :count
// h.float :duration

// t.hstore :data

