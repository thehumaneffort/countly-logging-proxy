var raven = require('raven');
var client = new raven.Client(process.env.RAVEN_DSN);

if (process.env.RAVEN_DSN) {
  client.patchGlobal();
  module.exports = {
    report: function() {
      client.captureException.apply(client, arguments);
    },
  };
} else {
  module.exports = {
    report: function() {

    },
  };
}
